package main
 
import (
        "log"
        "net/smtp"
       )
 
func main() {
    // Set up authentication information.
    auth := smtp.PlainAuth(
        "", // identity
        "testuser", // user name
        "123456", // password
        "smtp.163.com", // host
    )
    // Connect to the server, authenticate, set the sender and recipient,
    // and send the email all in one step.
    err := smtp.SendMail(
        "smtp.163.com:25", // addr eg: mail.example.com:25
        auth, // auth
        "test@163.com", // from
        []string{"test2@163.com"}, // to
        []byte("This is the email body."), // msg
    )
    if err != nil {
        log.Fatal(err)
    }
}